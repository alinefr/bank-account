# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bank_account,
  ecto_repos: [BankAccount.Repo]

# Configures the endpoint
config :bank_account, BankAccountWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jR+zptXrwY1FkaGWGJ7wzAv+sYBJ8yIQgfl2yGTofnH+lGJGF7bygBiENsLQghaD",
  render_errors: [view: BankAccountWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BankAccount.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "MP/dxwgy"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# configures Guardian
config :bank_account, BankAccount.Guardian,
  issuer: "BankAccount",
  ttl: {30, :days},
  allowed_drift: 2000,
  secret_key: %{"k" => "RyD4r-UxJb0lfeOLRUQ9yQ", "kty" => "oct"},
  serializer: BankAccount.Guardian

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Bank account

This is a GraphQL api to simulate bank transactions. It was done in an effort to learn Elixir.

## Known issues
While transactions are working as it should, there are some known issues, as I took only two days and without any previous elixir knowledge.

* Value amounts are integer. GraphQL only supports 32-bit. Decimal or [Long](https://github.com/graphql/graphql-spec/issues/73) may be prefered.
* Authentication is validated only for transaction queries. 
* Errors and exceptions are not properly handled.
* Missing tests.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Elixir
* PostgreSQL (it may work on different databases, but only tested in PSQL)

For Mac OSX

```
brew install elixir
brew install postgresql 
```
Check the [docs](https://wiki.postgresql.org/wiki/Homebrew) for postgresql setup in Mac OSX

For other systems, check [here](https://elixir-lang.org/install.html)

### Installing

Install dependencies
```
mix deps.get
```

Configure database by editing the following section in `config/dev.exs`:
```
# Configure your database
config :bank_account, BankAccount.Repo,
  username: "postgres",
  password: "postgres",
  database: "bank_account_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true, 
  pool_size: 10
```

Setup database and run migrations
```
mix ecto.setup
```

### Usage

Start server

```
mix phx.server
```

Open http://localhost:4000/graphiql in your browser

We need to create at least two users. For the first we are going to generate a token.

#### First user
If you don't want to use real cpf (and you shouldn't), the following command will generate a fake one for you to use

```
mix cpf.gen
```

In the browser, in the query window from graphiql, do like this for the first user:
```
mutation {
  createUser(cpf: "21455135402", password: "mypassword", name: "Leia Organa"){
    name
    cpf
  }
}
```

For this user, we are going to generate a token.
```
query {
  login(cpf: "21455135402", password: "mypassword"){
    token
  }
}
```

In graphiql, open HTTP HEADERS -> + add new header and fill up with:
```
Authorization | Bearer {your token generated in last step}
```

Create your first bank account:
```
mutation{
  createAccount(cpf: "21455135402"){
    id
  }
}
```

Write down your id. 
```
{
  "data": {
    "createAccount": {
      "id": "4"
    }
  }
}
```

#### Second user

User:
```
mutation {
  createUser(cpf: "83150798329", password: "mypassword", name: "Rey Skywalker"){
    name
    cpf
  }
}
```

Account:
```
mutation{
  createAccount(cpf: "83150798329"){
    id
  }
}
```

Returned id:
```
{
  "data": {
    "createAccount": {
      "id": "5"
    }
  }
}
```

#### Transactions

I (Leia Organa) want to deposit some money to my account first. R$ 100,00 (represented as integer)
```
mutation{
  deposit(accountId: 4, amount: 10000){
    status
  }
}
```


I want to check my balance
```
query {
  balance(accountId: 4){
    value
  }
}
```

And now I want to transfer R$ 50,00 to Ray Skywalker
```
mutation{
  transfer(sourceAccountId: 4, destinationAccountId:5, amount: 5000){
    status
  }
}
```

I could check again my balance
```
query {
  balance(accountId: 4){
    value
  }
}
```
It returns:
```
{
  "data": {
    "balance": {
      "value": 5000
    }
  }
}
```

## Built With

* [Phoenix Framework](https://www.phoenixframework.org/) - The web framework used
* [Absinthe](https://absinthe-graphql.org/) - GraphQL toolkit
* [Ecto](https://github.com/elixir-ecto/ecto) - Database ORM

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Aline Freitas** - [Aline Freitas](https://github.com/alinefr)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

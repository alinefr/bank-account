defmodule BankAccount.Guardian do
 use Guardian, otp_app: :bank_account
 alias BankAccount.Accounts

 def subject_for_token(user, _claims) do
  sub = to_string(user.cpf)
  {:ok, sub}
 end

 def resource_from_claims(claims) do
  user = claims["sub"] |> Accounts.get_user!
  {:ok,  user}
 end
end

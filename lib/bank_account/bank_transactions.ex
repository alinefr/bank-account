defmodule BankAccount.BankTransactions do
  alias BankAccount.Bank.Account

  @moduledoc """
  The BankTransactions context.
  """

  import Ecto.Query, warn: false
  alias BankAccount.Repo

  alias BankAccount.BankTransactions.Transaction

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list_transactions()
      [%Transaction{}, ...]

  """
  def list_transactions do
    Repo.all(Transaction)
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get_transaction!(123)
      %Transaction{}

      iex> get_transaction!(456)
      ** (Ecto.NoResultsError)

  """
  def get_transaction!(id), do: Repo.get!(Transaction, id)

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(%{field: value})
      {:ok, %Transaction{}}

      iex> create_transaction(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(attrs \\ %{}) do
    %Transaction{}
    |> Transaction.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a transaction.

  ## Examples

      iex> update_transaction(transaction, %{field: new_value})
      {:ok, %Transaction{}}

      iex> update_transaction(transaction, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_transaction(%Transaction{} = transaction, attrs) do
    transaction
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a transaction.

  ## Examples

      iex> delete_transaction(transaction)
      {:ok, %Transaction{}}

      iex> delete_transaction(transaction)
      {:error, %Ecto.Changeset{}}

  """
  def delete_transaction(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking transaction changes.

  ## Examples

      iex> change_transaction(transaction)
      %Ecto.Changeset{source: %Transaction{}}

  """
  def change_transaction(%Transaction{} = transaction) do
    Transaction.changeset(transaction, %{})
  end

  def deposit(account_id, amount, cpf, origin \\ "user deposited") do
    attrs = %{account_id: account_id, credit: amount, cpf: cpf, origin: origin}
    %Transaction{}
    |> Transaction.changeset(attrs)
    |> Repo.insert!()
    %{ok: true, status: "user deposited #{amount}"}
  end

  def withdraw(account_id, amount, cpf, origin \\ "user withdrawed") do
    attrs = %{account_id: account_id, debit: amount, cpf: cpf, origin: origin}
    account_cpf = Repo.get_by(Account, id: account_id).cpf

    balance = balance(account_id, cpf).value

    cond do
      cpf == account_cpf ->
        if balance >= amount do
          %Transaction{}
          |> Transaction.changeset(attrs)
          |> Repo.insert!()
          %{ok: true, status: "user withdrew #{amount}"}
        else
          %{error: true, status: "insuficient funds"}
        end
      true -> %{error: true, status: "user unauthorized"}
    end
  end

  def transfer(s_account_id, d_account_id, amount, cpf) do
    source_origin = "transfered to #{d_account_id}"
    destination_origin = "transfered from #{d_account_id}"
    account_cpf = Repo.get_by(Account, id: s_account_id).cpf

    cond do
      account_exists?(s_account_id) &&
      account_exists?(d_account_id) &&
        cpf == account_cpf -> 
        withdraw = withdraw(s_account_id, amount, cpf, source_origin)
        if Map.has_key?(withdraw, :ok) do
          deposit = deposit(d_account_id, amount, cpf, destination_origin)
          if Map.has_key?(deposit, :ok) do
            %{ok: true, status: "transfered #{amount} from account: #{s_account_id} to account: #{d_account_id}"}
          else
            %{ok: true, status: "error transfering funds: #{deposit}"}
          end
        else
          %{ok: true, status: "insuficient funds"}
        end
      true -> %{status: "user unauthorized"}
    end
  end

  defp account_exists?(account_id) do
    Repo.exists?(Account, id: account_id)
  end


  def balance(account_id, cpf) do
    query_account = Repo.get_by(Account, id: account_id)

    cond do
      account_exists?(account_id) &&
        query_account != nil &&
        Map.has_key?(query_account, :cpf) &&
        cpf == query_account.cpf ->
          credit = Repo.one(from p in Transaction, select: sum(p.credit), where: p.account_id == ^account_id) || 0
          debit = Repo.one(from p in Transaction, select: sum(p.debit), where: p.account_id == ^account_id) || 0
          value = credit - debit
          %{value: value}
      true -> %{value: "user unauthorized"}
    end
  end
end

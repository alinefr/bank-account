defmodule BankAccount.BankTransactions.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  schema "transactions" do
    field :account_id, :integer
    field :credit, :integer
    field :debit, :integer
    field :origin, :string

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:account_id, :credit, :debit, :origin])
    |> validate_required([:account_id, :origin])
  end
end

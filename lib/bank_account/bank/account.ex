defmodule BankAccount.Bank.Account do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field :balance, :integer, default: 0
    belongs_to :users, BankAccount.Auth.User,
      references: :cpf,
      type: :string,
      foreign_key: :cpf

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:balance, :cpf])
    |> validate_required([:cpf])
  end
end

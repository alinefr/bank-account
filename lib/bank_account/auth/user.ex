defmodule BankAccount.Auth.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:cpf, :string, size: 14, autogenerate: false}
  schema "users" do
    field :name, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :token, :string
    has_one :accounts, BankAccount.Bank.Account,
      references: :cpf,
      foreign_key: :cpf

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :cpf, :password])
    |> validate_required([:name, :cpf, :password])
    |> validate_length(:name, min: 3, max: 100)
    |> validate_length(:password, min: 5, max: 20)
    |> put_password_hash()
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end

  def store_token_changeset(user, attrs) do
    user
    |> cast(attrs, [:token])
  end
end

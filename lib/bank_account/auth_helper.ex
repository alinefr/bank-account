defmodule BankAccount.AuthHelper do
  @moduledoc false

  import Comeonin.Bcrypt, only: [checkpw: 2]

  alias BankAccount.Repo
  alias BankAccount.Auth.User

  def login_with_cpf_pass(cpf, given_pass) do
    user = Repo.get_by(User, cpf: cpf)

    cond do
      user && checkpw(given_pass, user.password_hash) ->
        {:ok, user}

      user ->
        {:error, "Incorrect login credentials"}

      true ->
        {:error, :"User not found"}
    end
  end
end

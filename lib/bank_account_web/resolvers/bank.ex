defmodule BankAccountWeb.Resolvers.Bank do
  alias BankAccount.Bank

  def accounts(_, _, _) do
    {:ok, Bank.list_accounts()}
  end

  def account(_, %{id: id}, _) do
    {:ok, Bank.get_account!(id)}
  end

  def create_account(_, %{cpf: cpf}, _) do
    {:ok, Bank.create_account!(cpf)}
  end
end

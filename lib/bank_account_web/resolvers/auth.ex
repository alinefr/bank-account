defmodule BankAccountWeb.Resolvers.Auth do
  alias BankAccount.Auth
  import BankAccount.AuthHelper

  def users(_, _) do
    {:ok, Auth.list_users()}
  end

  def user(%{cpf: cpf}, %{context: %{current_user: _current_user}}) do
    {:ok, Auth.get_user!(cpf)}
  end

  def user(_, _) do
    {:error, "Not Authorized"}
  end


  def login(%{cpf: cpf, password: password}, _) do
    with {:ok, user } <- login_with_cpf_pass(cpf, password),
         {:ok, jwt, _} <- BankAccount.Guardian.encode_and_sign(user),
         {:ok, _ } <- Auth.store_token(user, jwt) do
      {:ok, %{token: jwt}}
    end
  end

  def create_user(_, attrs, _) do
    {:ok, Auth.create_user!(attrs)}
  end
end

defmodule BankAccountWeb.Resolvers.BankTransactions do
  alias BankAccount.BankTransactions

  def deposit(%{account_id: account_id, amount: amount}, %{context: %{current_user: current_user}}) do
    cpf = current_user.cpf
    {:ok, BankTransactions.deposit(account_id, amount, cpf)}
  end

  def deposit(_, _) do
    {:error, "Not authorized."}
  end

  def withdraw(%{account_id: account_id, amount: amount}, %{context: %{current_user: current_user}}) do
    cpf = current_user.cpf
    {:ok, BankTransactions.withdraw(account_id, amount, cpf)}
  end

  def withdraw(_, _) do
    {:error, "Not authorized."}
  end

  def transfer(%{source_account_id: s_account_id, destination_account_id: d_account_id, amount: amount}, %{context: %{current_user: current_user}}) do
    cpf = current_user.cpf
    {:ok, BankTransactions.transfer(s_account_id, d_account_id, amount, cpf)}
  end

  def transfer(_,_) do
    {:error, "Not authorized."}
  end

  def balance(%{account_id: account_id}, %{context: %{current_user: current_user}}) do
    cpf = current_user.cpf
    {:ok, BankTransactions.balance(account_id, cpf)}
  end

  def balance(_, _) do
    {:error, "Not authorized."}
  end
end

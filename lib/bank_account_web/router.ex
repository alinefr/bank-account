defmodule BankAccountWeb.Router do
  use BankAccountWeb, :router

  pipeline :api do
    plug BankAccount.Context
  end

  scope "/" do
    pipe_through :api

    forward "/api", Absinthe.Plug,
      schema: BankAccountWeb.Schema.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: BankAccountWeb.Schema.Schema,
      interface: :playground
  end
end

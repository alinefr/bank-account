defmodule BankAccountWeb.Schema.Schema do
  use Absinthe.Schema

  query do
    @desc "Get a list of users"
    field :users, list_of(:user) do
      resolve &BankAccountWeb.Resolvers.Auth.users/2
    end

    @desc "Get an user by its cpf"
    field :user, :user do
      arg :cpf, non_null(:string)
      resolve &BankAccountWeb.Resolvers.Auth.user/2
    end

    @desc "Login"
    field :login, :session do
      arg :cpf, non_null(:string)
      arg :password, non_null(:string)
      resolve &BankAccountWeb.Resolvers.Auth.login/2
    end

    @desc "Get a list of accounts"
    field :accounts, list_of(:account) do
      arg :id, :string
      resolve &BankAccountWeb.Resolvers.Bank.accounts/3
    end

    @desc "Get an account by its cpf"
    field :account, :account do
      arg :cpf, non_null(:string)
      resolve &BankAccountWeb.Resolvers.Bank.account/3
    end

    @desc "Get balance amount"
    field :balance, :balance do
      arg :account_id, non_null(:id)
      resolve &BankAccountWeb.Resolvers.BankTransactions.balance/2
    end

    mutation do
      @desc "Create a new user"
      field :create_user, :user do
        arg :cpf, non_null(:string)
        arg :name, non_null(:string)
        arg :password, non_null(:string)

        resolve &BankAccountWeb.Resolvers.Auth.create_user/3
      end

      @desc "Create a new account"
      field :create_account, :account do
        arg :cpf, non_null(:string)
        resolve &BankAccountWeb.Resolvers.Bank.create_account/3
      end

      @desc "Deposit amount"
      field :deposit, :transaction do
        arg :account_id, non_null(:id)
        arg :amount, non_null(:integer)

        resolve &BankAccountWeb.Resolvers.BankTransactions.deposit/2
      end

      @desc "Withdraw amount"
      field :withdraw, :transaction do
        arg :account_id, non_null(:id)
        arg :amount, non_null(:integer)

        resolve &BankAccountWeb.Resolvers.BankTransactions.withdraw/2
      end

      @desc "Transfer amount"
      field :transfer, :transaction do
        arg :source_account_id, non_null(:id)
        arg :destination_account_id, non_null(:id)
        arg :amount, non_null(:integer)

        resolve &BankAccountWeb.Resolvers.BankTransactions.transfer/2
      end
    end
  end

  object :user do
    field :cpf, non_null(:string)
    field :name, non_null(:string)
    field :password, non_null(:string)
  end

  object :session do
    field(:token, :string)
  end

  object :balance do
    field(:value, :integer)
  end

  object :account do
    field :id, non_null(:id)
    field :cpf, non_null(:string)
    field :balance, non_null(:integer)
  end

  object :transaction do
    field :status, :string
  end
end

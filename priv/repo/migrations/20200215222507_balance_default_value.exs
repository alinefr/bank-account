defmodule BankAccount.Repo.Migrations.BalanceDefaultValue do
  use Ecto.Migration

  def up do
    alter table(:accounts) do
      modify :balance, :integer, default: 0
    end
  end

  def down do
    alter table(:accounts) do
      modify :balance, :integer, default: nil
    end
  end
end

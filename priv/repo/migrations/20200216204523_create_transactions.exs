defmodule BankAccount.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :account_id, :integer
      add :credit, :integer
      add :debit, :integer
      add :origin, :text

      timestamps()
    end

  end
end

defmodule BankAccount.Repo.Migrations.UseCpfPrimaryKey do
  use Ecto.Migration

  def change do
    execute "ALTER TABLE accounts DROP CONSTRAINT accounts_user_id_fkey"
    alter table(:users) do
      remove(:id)
      modify(:cpf, :string, size: 14, primary_key: true)
    end
  end
end

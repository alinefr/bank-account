defmodule BankAccount.Repo.Migrations.ReplaceIdByCpfAccounts do
  use Ecto.Migration

  def change do
    drop index(:accounts, [:user_id])
    alter table(:accounts) do
      remove(:user_id)
      add(:cpf, references(:users, on_delete: :nothing, type: :string, column: :cpf))
    end
  end
end

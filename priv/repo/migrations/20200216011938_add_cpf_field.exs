defmodule BankAccount.Repo.Migrations.AddCpfField do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :cpf, :string, size: 14
    end
  end
end
